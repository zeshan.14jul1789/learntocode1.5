import pyttsx3 as psx
import time as t

__engine = psx.init()


def playPod(sound, speed):
    __engine.setProperty('rate', __engine.getProperty('rate') * speed)
    __engine.say(sound)
    __engine.runAndWait()
    __engine.setProperty('rate', 200)

def pausePod(time):
    t.sleep(time)

